export class StringExtras {

    static empty = "";

    static isNullOrWhitespace(input) {
        if (typeof input === 'undefined' || input == null) {
            return true;
        }
        return input.replace(/\s/g, '').length < 1;
    }

    static capitalize(value, lower) {
        return (lower ? value.toLowerCase() : value).replace(/(?:^|\s|["'([{])+\S/g, function (match) { return match.toUpperCase() });
    }

}