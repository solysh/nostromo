export class MathExtras {

    static clamp(value, min, max) {
        return Math.min(Math.max(value, min), max);
    }

    static isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

}