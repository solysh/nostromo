export class RandomExtras {

    static getRandomAlphabetString(length) {
        var alphabet = new Array(26).fill(1).map((_, i) => String.fromCharCode(65 + i));
        return new Array(length).fill(1).map((_, i) => alphabet[RandomExtras.getRandomNumber(0, 25)]);
    }

    static getRandomNumberString(length) {
        var numbers = new Array(10).fill(1).map((_, i) => String.fromCharCode(48+ i));
        return new Array(length).fill(1).map((_, i) => numbers[RandomExtras.getRandomNumber(0, 9)]);
    }

    static getRandomNumber(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

}