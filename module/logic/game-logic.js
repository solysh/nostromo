
export class GameLogic {
    static isCritical(value) {
        if (value === 0) {
            return true;
        }
        if ((value % 11) === 0) {
            return true;
        }
        return false;
    }
}