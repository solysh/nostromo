import { TemplatePaths } from "../constants/template-paths.js";

export class ChatLogic {
    static async renderStatRollResults(rollData) {
        ChatLogic.renderTemplateMessage(rollData, TemplatePaths.statRoll);
    }

    static async renderSaveRollResults(rollData) {
        ChatLogic.renderTemplateMessage(rollData, TemplatePaths.saveRoll);
    }

    static async renderWeaponDamageRollResults(rollData) {
        ChatLogic.renderTemplateMessage(rollData, TemplatePaths.weaponDamageRoll);
    }

    static async renderPanicRollResults(rollData) {
        ChatLogic.renderTemplateMessage(rollData, TemplatePaths.panicRoll);
    }

    static async renderWeaponReload(rollData) {
        ChatLogic.renderTemplateMessage(rollData, TemplatePaths.weaponReload);
    }

    static async renderTemplateMessage(data, templatePath) {
        var template = await getTemplate(templatePath);
        var message = template(data);
        ChatLogic.renderMessage(message);
    }

    static async renderMessage(message) {
        ChatMessage.create({
            content: message
        }, {});
    }
}