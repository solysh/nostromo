﻿import { MathExtras } from "../extras/math-extras.js";
import { StringExtras } from "../extras/string-extras.js";
import { PsuedoIdGenerator } from "../psuedo-id-generator.js";
import { NOSTROMO } from "../constants/config.js";
import { GameLogic } from "../logic/game-logic.js";
import { ChatLogic } from "../logic/chat-logic.js";
import { FormLogic } from "../logic/form-logic.js";

export class NostromoActorSheet extends ActorSheet {
    constructor(...args) {
        super(...args);
    }

    checkSystemId(playerData)
    {
        if (!StringExtras.isNullOrWhitespace(playerData.universalSystemId)) {
            return;
        }
        this.setSystemId(playerData);
    }

    setSystemId(playerData) {
        playerData.universalSystemId = PsuedoIdGenerator.generatePsuedoId(NOSTROMO.idTemplate);
        this.actor.update({ "data.universalSystemId": playerData.universalSystemId });
    }

    getInventoryByType(items, typeName) {
        return _.where(items, { type: typeName });
    }

    getInventory(items){
        const inventory = {};
        inventory.weapons = this.getInventoryByType(items, "weapon");
        inventory.armor = this.getInventoryByType(items, "armor");
        inventory.items = this.getInventoryByType(items, "item");

        this.setWeaponMetaData(inventory.weapons);

        return inventory;
    }

    setWeaponMetaData(weapons) {
        for (let i = 0; i < weapons.length; i++) {

            let data = weapons[i].data;
            weapons[i].metaData = weapons[i].metaData || {};
            weapons[i].metaData.canAttack = false;
            weapons[i].metaData.canReload = false;

            if (!(data.equipped) || !(data.equipped.value)) {
                continue;
            }

            if (!(data.usesAmmo) || !(data.usesAmmo.value)) {
                weapons[i].metaData.canAttack = true;
                continue;
            }

            if (data.loadedAmmo
                && data.loadedAmmo.value > 0) {
                weapons[i].metaData.canAttack = true;
            }

            if (data.loadedAmmo
                && data.shots
                && data.loadedAmmo.value == data.shots.value) {
                continue;
            }

            if (data.remainingAmmo
                && data.remainingAmmo.value > 0) {
                weapons[i].metaData.canReload = true;
            }
        }
    }

    getArmorBonus(items) {
        var armor = this.getInventoryByType(items, "armor");
        const equippedArmor = _.filter(armor, function (item) {
            return item.data.equipped.value === true;
        });

        let armorBonus = 0;
        if (equippedArmor)
        {
            armorBonus = _.reduce(equippedArmor, function (memo, item) {
                let bonus = item.data.bonus.value;
                if (MathExtras.isNumeric(bonus))
                {
                    return memo + bonus;
                }
                return memo;
            }, 0);
            return armorBonus;
        }
        return 0;
    }

    calculateHealthColor(percentage) {
        var green = percentage * 255 * 2;
        var red = (255 - (percentage * 255)) * 2;
        var blue = 50;

        var calculatedColor = {
            "red": MathExtras.clamp(red, 0, 255),
            "green": MathExtras.clamp(green, 0, 255),
            "blue": MathExtras.clamp(blue, 0, 255)
        };
        return calculatedColor;
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);
        // Everything below here is only needed if the sheet is editable
        if (!this.options.editable) return;

        // Update Inventory Item
        html.find('.item-edit').click(ev => {
            const li = $(ev.currentTarget).parents(".item");
            const item = this.actor.getOwnedItem(li.data("itemId"));
            item.sheet.render(true);
        });

        // Delete Inventory Item
        html.find('.item-delete').click(ev => {
            const li = $(ev.currentTarget).parents(".item");
            this.actor.deleteOwnedItem(li.data("itemId"));
            li.slideUp(200, () => this.render(false));
        });

        // Attack with weapon
        html.find('.weapon-attack').click(ev => 
            this.onWeaponAttack(ev));

        html.find('.weapon-reload').click(ev =>
            this.onWeaponReload(ev));

        html.find('.item-create').click(ev =>
            this._onItemCreate(ev));

        html.find(".stat-roll").click(ev =>
            this.rollStatCheck(ev));

        html.find(".aliased-stat-roll").click(ev =>
            this.rollAliasedStatCheck(ev));

        html.find(".aliased-save-roll").click(ev =>
            this.rollAliasedStatCheck(ev));

    }


    async onWeaponReload(event) {
        const li = $(event.currentTarget).parents(".item");
        const item = this.actor.getOwnedItem(li.data("itemId"));
        const itemData = item.data.data;

        itemData.remainingAmmo = itemData.remainingAmmo || {};
        const remainingAmmo = itemData.remainingAmmo.value || 0;

        itemData.shots = itemData.shots || {};
        const shots = itemData.shots.value || 0;

        itemData.loadedAmmo = itemData.loadedAmmo || {};
        const loaded = itemData.loadedAmmo.value || 0;

        if (remainingAmmo == 0) {
            console.log("no spare ammo");
            return;
        }

        if (shots == 0) {
            console.log("shots not set");
            return;
        }

        let difference = shots - loaded;

        if (difference == 0) {
            console.log("weapon is full");
            return;
        }

        const ammoToLoad = Math.min(difference, remainingAmmo)
        const updatedLoadedAmmo = ammoToLoad + loaded;
        const updatedRemainingAmmo = remainingAmmo - ammoToLoad;
        item.update({ "data.loadedAmmo.value": updatedLoadedAmmo });
        item.update({ "data.remainingAmmo.value": updatedRemainingAmmo });

        let name = item.name;
        if (itemData.make && itemData.make.value != null && itemData.make.value.trim() != "") {
            name = itemData.make.value + " " + name;
        }

        var chatData = {
            name: name
        };

        ChatLogic.renderWeaponReload(chatData);

        this.actor.sheet.render(true);
    }

    onWeaponAttack(event) {
        const li = $(event.currentTarget).parents(".item");
        const item = this.actor.getOwnedItem(li.data("itemId"));
        const itemData = item.data.data;
        if (!(itemData.usesAmmo) || !(itemData.usesAmmo.value)) {
            this.rollWeaponDamage(itemData, item.name);
            return;
        }

        if (!(itemData.loadedAmmo) || itemData.loadedAmmo.value == 0) {
            console.log("weapon is empty");
            return;
        }


        let ammoUsed = 1;
        let formula = "1d6";

        if (itemData.burst && itemData.burst.value) {
            formula = itemData.burst.value;
        }

        if (itemData.automatic && itemData.automatic.value) {
            const cleanFormula = Roll.cleanFormula(formula);
            let dice = new Roll(cleanFormula);
            dice.roll();
            ammoUsed = Math.min(dice.total, itemData.loadedAmmo.value);
        }

        let result = itemData.loadedAmmo.value - ammoUsed; 

        item.update({ "data.loadedAmmo.value": result });

        this.rollWeaponDamage(itemData, item.name, ammoUsed);

        this.actor.sheet.render(true);
    }


    rollWeaponDamage(itemData, itemName, ammoUsed) {

        const damageFormula = itemData.damage.value;

        const cleanFormula = Roll.cleanFormula(damageFormula);
        let dice = new Roll(cleanFormula);
        dice.roll();

        let name = itemName;
        if (itemData.make && itemData.make.value != null && itemData.make.value.trim() != "") {
            name = itemData.make.value + " " + name;
        }

        var rollData = {
            name: name,
            ammo: ammoUsed,
            formula: cleanFormula,
            result: dice.result,
            total: dice.total
        };

        ChatLogic.renderWeaponDamageRollResults(rollData);
    }

    rollAliasedSaveCheck(event) {
        event.preventDefault();
        const header = event.currentTarget;
        const player = super.getData()
        const data = player.data;
        const statName = header.dataset.statName;
        const aliasName = header.dataset.statAlias;
        const stat = data.stats[statName];
        const aliasStat = data.saves[aliasName];

        let value = 0;
        let bonus = 0;
        let penalty = 0;

        if (stat && stat.value) {
            value = stat.value;
        }

        if (aliasStat && aliasStat.bonus) {
            bonus = aliasStat.bonus;
        }

        if (aliasStat && aliasStat.penalty) {
            penalty = aliasStat.penalty;
        }

        let dice = new Roll("1d100");
        dice.roll();

        this.notifyRollResults(aliasName, value, bonus, penalty, dice.total);

    }

    rollAliasedStatCheck(event) {
        event.preventDefault();
        const header = event.currentTarget;
        const player = super.getData()
        const data = player.data;
        const statName = header.dataset.statName;
        const aliasName = header.dataset.statAlias;
        const stat = data.stats[statName];
        const aliasStat = data.stats[aliasName];

        let value = 0;
        let bonus = 0;
        let penalty = 0;

        if (stat && stat.value) {
            value = stat.value;
        }

        if (aliasStat && aliasStat.bonus) {
            bonus = aliasStat.bonus;
        }

        if (aliasStat && aliasStat.penalty) {
            penalty = aliasStat.penalty;
        }

        let dice = new Roll("1d100");
        dice.roll();

        this.notifyRollResults(aliasName, value, bonus, penalty, dice.total);

    }

    rollStatCheck(event) {
        event.preventDefault();
        const header = event.currentTarget;
        const player = super.getData()
        const data = player.data;
        const statName = header.dataset.statName;
        const stat = data.stats[statName];

        let value = 0;
        let bonus = 0;
        let penalty = 0;

        if (stat && stat.value) {
            value = stat.value;
        }

        if (stat && stat.bonus) {
            bonus = stat.bonus;
        }

        if (stat && stat.penalty) {
            penalty = stat.penalty;
        }

        let dice = new Roll("1d100");
        dice.roll();

        this.notifyRollResults(statName, value, bonus, penalty, dice.total);
    }


    notifyRollResults(name, value, bonus, penalty, result) {
        const success = (result < value);
        const critical = GameLogic.isCritical(result);
        
        const rollData = {
            critical: critical,
            success: success,
            name: StringExtras.capitalize(name),
            value: value,
            bonus: bonus,
            penalty: penalty,
            result: result,
            total: (value + bonus - penalty) 
        }

        ChatLogic.renderStatRollResults(rollData);
    }

    /** @override */
    _updateObject(event, formData) {
        const payload = FormLogic.unflatten(formData);

        if (!(payload.item))
        {
            return this.object.update(formData);
        }
        var items = [];
        var keys = Object.keys(payload.item);
        for (let i = 0; i < keys.length; i++) {
            let key = keys[i];
            items.push(payload.item[key]);
        }

        const updated = this.actor.updateEmbeddedEntity("OwnedItem", items)
        return this.object.update(formData);
    }

    /**
      * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
      * @param {Event} event The originating click event
      */
    _onItemCreate(event) {
        event.preventDefault();
        const header = event.currentTarget;
        const type = header.dataset.itemType;
        const itemData = {
            name: `New ${type.capitalize()}`,
            type: type,
            data: duplicate(header.dataset)
        };
        delete itemData.data['type'];
        return this.actor.createOwnedItem(itemData);
    }

    getNewItemData(event) {
        var header = this.getHeader(event);
        return header.dataset;
    }

    getItemType(event) {
        var header = this.getHeader(event);
        return header.dataset.itemType;
    }

    getHeader(event) {
        return event.currentTarget;
    }
}