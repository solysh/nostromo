import { NostromoActorSheet } from "./nostromo-actor-sheet.js";
import { MathExtras } from "../extras/math-extras.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {NostromoActorSheet}
 */
export class ShipSheet extends NostromoActorSheet  {
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["nostromo", "sheet", "actor", "ship"],
            template: "systems/nostromo/templates/actor/ship-sheet.html",
            width: 700,
            height: "auto",
            resizable: false,
            tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description" },
            { navSelector: ".nostromo-info-tabs", contentSelector: ".nostromo-character-info-sheet", initial: "info" }
            ]
        });
    }

    /** @override */
    getData() {
        const data = super.getData();
        data.dtypes = ["String", "Number", "Boolean"];
        data.modules = this.getInventoryByType(data.items, "shipModule");
        data.weapons = this.getInventoryByType(data.items, "shipWeapon");
        data.inventory = {};
        data.inventory.items = this.getInventoryByType(data.items, "item");
        data.officers = this.getInventoryByType(data.items, "officer");
        return data;
    }
}