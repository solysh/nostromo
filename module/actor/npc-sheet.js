import { NostromoActorSheet } from "./nostromo-actor-sheet.js";
import { MathExtras } from "../extras/math-extras.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class NpcSheet extends NostromoActorSheet{

    /** @override */
    static get defaultOptions() {

        return mergeObject(super.defaultOptions, {
            classes: ["nostromo", "sheet", "actor", "npc"],
            template: "systems/nostromo/templates/actor/npc-sheet.html",
            width: 700,
            height: "auto",
            resizable: false,
            tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description" },
            { navSelector: ".nostromo-info-tabs", contentSelector: ".nostromo-character-info-sheet", initial: "info" }
            ]
        });
    }

    /** @override */
    getData() {
        const data = super.getData();
        data.dtypes = ["String", "Number", "Boolean"];
        data.skills = this.getInventoryByType(data.items, "skill");
        data.inventory = this.getInventory(data.items);
        data.derivedStats = data.derivedStats || {};
        //data.derivedStats.armorBonus = this.getArmorBonus(data.items);

        this.checkSystemId(data.actor.data);
        this.validateHits(data.actor.data);
        return data;
    }

    validateHits(playerData) {
        if (playerData.attributes.hits.value <= playerData.attributes.hits.max) {
            return;
        }
        const hitsValue = MathExtras.clamp(playerData.attributes.hits.value, 0, playerData.attributes.hits.max);
        this.actor.update({ "data.attributes.hits.value": hitsValue });
    }
}
