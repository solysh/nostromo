
import { NostromoActorSheet } from "./nostromo-actor-sheet.js";
import { MathExtras } from "../extras/math-extras.js";
import { ChatLogic } from "../logic/chat-logic.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {NostromoActorSheet}
 */
export class CharacterSheet extends NostromoActorSheet {
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["nostromo", "sheet", "actor", "character"],
            template: "systems/nostromo/templates/actor/character-sheet.html",
            width: 700,
            height: "auto",
            resizable: false,
            tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description" },
            { navSelector: ".nostromo-info-tabs", contentSelector: ".nostromo-character-info-sheet", initial: "info" }
            ]
        });
    }

    /** @override */
    getData() {
        const data = super.getData();
        data.dtypes = ["String", "Number", "Boolean"];
        data.skills = this.getInventoryByType(data.items, "skill");
        data.inventory = this.getInventory(data.items);
        //data.derivedStats = data.derivedStats || {};
        //data.derivedStats.armorBonus = this.getArmorBonus(data.items);
        //const armorBonus = this.getArmorBonus(data.items);
        this.checkSystemId(data.actor.data);
        this.validateHealth(data.actor.data);

        return data;
    }

    validateHealth(playerData) {
        if (playerData.attributes.health.value <= playerData.attributes.health.max) {
            return;
        }
        const healthValue = MathExtras.clamp(playerData.attributes.health.value, 0, playerData.attributes.health.max);
        this.actor.update({ "data.attributes.health.value": healthValue });
    }

    activateListeners(html) {
        super.activateListeners(html);

        if (!this.options.editable) return;

        html.find(".save-roll").click(ev =>
            this.rollSaveCheck(ev));

        html.find(".panic-roll").click(ev =>
            this.rollPanicCheck(ev));
    }

    rollPanicCheck(event) {
        event.preventDefault();
        const player = super.getData();
        const data = player.data;

        let stress = 0;

        if (data.attributes && data.attributes.stress && data.attributes.stress.value > 0) {
            stress = data.attributes.stress.value;
        }

        let dice = new Roll("2d10");
        dice.roll();

        let success = dice.total > stress;

        var rollData = {
            value: stress,
            total: dice.total,
            success: success
        };

        ChatLogic.renderPanicRollResults(rollData);

        if (!success) {
            return;
        }


        if (stress == 0) {
            return
        }

        stress -= 1;

        this.actor.update({ "data.attributes.stress.value": stress });
        this.actor.sheet.render(true);
    }


    rollSaveCheck(event) {
        event.preventDefault();
        const header = event.currentTarget;
        const player = super.getData()
        const data = player.data;
        const saveName = header.dataset.statName;
        const save = data.saves[saveName];

        let value = 0;
        let bonus = 0;
        let penalty = 0;

        if (save && save.value) {
            value = save.value;
        }

        if (save && save.bonus) {
            bonus = save.bonus;
        }

        if (save && save.penalty) {
            penalty = save.penalty;
        }

        let dice = new Roll("1d100");
        dice.roll();

        this.notifyRollResults(saveName, value, bonus, penalty, dice.total);

        if (dice.total < value) {
            return;
        }

        let stress = 0;

        if (data.attributes && data.attributes.stress && data.attributes.stress.value > 0) {
            stress = data.attributes.stress.value;
        }

        stress += 1;

        this.actor.update({ "data.attributes.stress.value": stress });
        this.actor.sheet.render(true);
    }
}
