
export class TemplatePaths {
    static statRoll = "systems/nostromo/templates/chat/stat-roll.html";
    static saveRoll = "systems/nostromo/templates/chat/save-roll.html";
    static panicRoll = "systems/nostromo/templates/chat/panic-roll.html";
    static weaponDamageRoll = "systems/nostromo/templates/chat/weapon-damage-roll.html";
    static weaponReload = "systems/nostromo/templates/chat/weapon-reload.html";
}