/**
 * Define a set of template paths to pre-load
 * Pre-loaded templates are compiled and cached for fast access when rendering
 * @return {Promise}
 */

import { TemplatePaths } from "./constants/template-paths.js";


export const preloadHandlebarsTemplates = async function () {

    // Define template paths to load
    const templatePaths = [
        "systems/nostromo/templates/actor/partial/info.html",
        "systems/nostromo/templates/actor/partial/condition.html",
        "systems/nostromo/templates/actor/partial/stats.html",
        "systems/nostromo/templates/actor/partial/saves.html",
        "systems/nostromo/templates/actor/partial/skills.html",
        "systems/nostromo/templates/actor/partial/armor.html",
        "systems/nostromo/templates/actor/partial/weapons.html",
        "systems/nostromo/templates/actor/partial/items.html",
        "systems/nostromo/templates/partial/footer.html", 
        "systems/nostromo/templates/actor/partial/mercenary-stats.html",
        "systems/nostromo/templates/actor/partial/mercenary-saves.html",
        "systems/nostromo/templates/actor/partial/mercenary-mstats.html",
        "systems/nostromo/templates/actor/partial/npc-stats.html",
        "systems/nostromo/templates/actor/partial/npc-mstats.html",
        "systems/nostromo/templates/item/partial/critical-effects.html",
        "systems/nostromo/templates/actor/partial/ship-stats.html",
        "systems/nostromo/templates/actor/partial/ship-modules.html",
        "systems/nostromo/templates/actor/partial/ship-officers.html",
        "systems/nostromo/templates/actor/partial/ship-weapons.html",
        TemplatePaths.statRoll,
        TemplatePaths.panicRoll,
        TemplatePaths.weaponDamageRoll,
        TemplatePaths.weaponReload,
        TemplatePaths.saveRoll

    ];

    // Load the template parts
    return loadTemplates(templatePaths);
};
