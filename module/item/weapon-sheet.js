import { UuidGenerator } from "../uuid-generator.js";
import { FormLogic } from "../logic/form-logic.js";
import { GameLogic } from "../logic/game-logic.js";
import { NostromoItemSheet } from "./nostromo-item-sheet.js";

export class WeaponSheet extends NostromoItemSheet {

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["nostromo", "sheet", "weapon"],
            template: "systems/nostromo/templates/item/weapon-sheet.html",
            width: 600,
            height: "auto",
            resizable: false,
            tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description" },
            { navSelector: ".nostromo-info-tabs", contentSelector: ".nostromo-character-info-sheet", initial: "info" }]
        });
    }

    /** @override */
    _updateObject(event, formData) {
        //unflatten form data
        const payload = FormLogic.unflatten(formData);

        //if nothing is listed under the crit namspace, then submit the data normally
        if (!(payload.crit)) {
            return this.object.update(formData);
        }

        //instantite new list of critical effects
        let criticalEffects = [];

        let keys = Object.keys(payload.crit);
        for (let i = 0; i < keys.length; i++) {
            let key = keys[i];
            criticalEffects.push(payload.crit[key]);
        }

        this.item.update({ "data.criticalEffects": criticalEffects });
        return this.object.update(formData);
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);

        // Everything below here is only needed if the sheet is editable
        if (!this.options.editable) return;

        // Delete Crit Effect
        html.find('.crit-delete').click(ev => this.onCritDelete(ev));

        //Create Crit Effect
        html.find('.crit-create').click(ev => this.onCritCreate(ev));
    }

    async onCritDelete(ev) {
        const tag = $(ev.currentTarget).parents(".crit");
        var criticalId = tag.data("critId");

        const itemData = this.item.data.data;

        let criticalEffects = duplicate(itemData.criticalEffects);

        criticalEffects = criticalEffects.filter(criticalEffect => {
            return criticalEffect._id !== criticalId;
        });

        this.item.update({ "data.criticalEffects": criticalEffects });
        this.item.sheet.render(true);
    }

    async onCritCreate(event) {
        event.preventDefault();
        const critical = {
            _id: UuidGenerator.uuidv4(),
            name: "New Critical"
        };

        const itemData = this.item.data.data;
        const criticalEffects = duplicate(itemData.criticalEffects);
        if (!Array.isArray(criticalEffects)) {
            criticalEffects = []
        }
        criticalEffects.push(critical);
        this.item.update({ "data.criticalEffects": criticalEffects });
        this.item.sheet.render(true);
    }
}
