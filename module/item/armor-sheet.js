import { NostromoItemSheet } from "./nostromo-item-sheet.js";

export class ArmorSheet extends NostromoItemSheet {

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["nostromo", "sheet", "armor"],
            template: "systems/nostromo/templates/item/armor-sheet.html",
            width: 600,
            height: "auto",
            resizable: false,
            tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description" },
            { navSelector: ".nostromo-info-tabs", contentSelector: ".nostromo-character-info-sheet", initial: "info" }]
        });
    }
}
