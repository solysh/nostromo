
import { RandomExtras } from "./extras/random-extras.js";

export class PsuedoIdGenerator {

    static generatePsuedoId = function (template) {
        if (!(template)) {
            return null;
        }
        var generatedIdArray = new Array(template.length + 1)
            .fill(1)
            .map((_, i) => {
                if (template[i] == "@") {
                    return RandomExtras.getRandomAlphabetString(1);
                }
                if (template[i] == "#") {
                    return RandomExtras.getRandomNumberString(1);
                }
                return template[i];
            });
        return generatedIdArray.join('');
    }

}
