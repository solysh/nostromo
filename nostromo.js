/**
 * The Nostromo game system for Foundry Virtual Tabletop
 * Author: Kotzwinkle
 * Software License: MIT
 * Repository: https://gitlab.com/kotzwinkle/nostromo.git
 * Issue Tracker: https://gitlab.com/kotzwinkle/nostromo/issues
 */

// Import Modules
import { NOSTROMO } from "./module/constants/config.js";
import { CharacterSheet } from "./module/actor/character-sheet.js";
import { MercenarySheet } from "./module/actor/mercenary-sheet.js";
import { NpcSheet } from "./module/actor/npc-sheet.js";
import { ShipSheet } from "./module/actor/ship-sheet.js";
import { SimpleItemSheet } from "./module/item/item-sheet.js";
import { SkillSheet } from "./module/item/skill-sheet.js";
import { ArmorSheet } from "./module/item/armor-sheet.js";
import { WeaponSheet } from "./module/item/weapon-sheet.js";
import { ShipModuleSheet } from "./module/item/ship-module-sheet.js";
import { ShipWeaponSheet } from "./module/item/ship-weapon-sheet.js";
import { preloadHandlebarsTemplates } from "./module/templates.js";


/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */

Hooks.once("init", async function () {

    console.log('%c%s', 'color:#FFCC00; background-color:black;', '💀 Nostromo 💀 | Initializing Nostromo System...');

	/**
	 * Set an initiative formula for the system
	 * @type {String}
	 */
    CONFIG.Combat.initiative = {
        formula: "1d100",
        decimals: 2
    };

    // Register sheet application classes
    Actors.unregisterSheet("core", ActorSheet);
    Actors.registerSheet("nostromo", CharacterSheet, { types: ["character"], makeDefault: true });
    Actors.registerSheet("nostromo", MercenarySheet, { types: ["mercenary"], makeDefault: true });
    Actors.registerSheet("nostromo", NpcSheet, { types: ["npc"], makeDefault: true });
    Actors.registerSheet("nostromo", ShipSheet, { types: ["ship"], makeDefault: true });

    Items.unregisterSheet("core", ItemSheet);
    Items.registerSheet("nostromo", SimpleItemSheet, { types: ["item"], makeDefault: true });
    Items.registerSheet("nostromo", SkillSheet, { types: ["skill"], makeDefault: true });
    Items.registerSheet("nostromo", ArmorSheet, { types: ["armor"], makeDefault: true });
    Items.registerSheet("nostromo", WeaponSheet, { types: ["weapon"], makeDefault: true });
    Items.registerSheet("nostromo", ShipModuleSheet, { types: ["shipModule"], makeDefault: true });
    Items.registerSheet("nostromo", ShipWeaponSheet, { types: ["shipWeapon"], makeDefault: true });

    // Preload Handlebars Templates
    preloadHandlebarsTemplates();
});
